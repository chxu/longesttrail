/*******These codes are for the classes Node_Info and Cycle mentioned in Node_Info.h******************/

#include "Header.h"
#include <algorithm>
#include <iostream>
#include "Node_Info.h"
using namespace std;
//To create a cycle which is contains directed edge (n, next)
Cycle::Cycle(int n, int l, int next, std::vector<int> p){
	node=n;
	length=l;
	nextnode=next;
	path=p;
}

//To get the length of the cycle
int Cycle::Getlength(){return length;}

//To get the nodes in the cycle
std::vector<int> Cycle::Getpath(){return path;}
	
//To create a subgraph (subtree) where the root is the node "number"
Node_Info::Node_Info(int number){
	number_node=number;
	l1=0;l2=0;
}

void Node_Info::Setl1(int v){
	l1=v;
}

// To save the longest trail in the subgraph
void Node_Info::Save_Inverse_Edges(int endpoint,int pass, std::pair<int,int> edge){
	std::pair<int, int> key(endpoint, pass);
	std::map<std::pair<int,int>,std::set<pair<int,int> > >::iterator it;
	it=Inverse_Edges.find(key);
	if(it!=Inverse_Edges.end()){
		it->second.insert(edge);
	}
	else{
		set<std::pair<int,int> > Edgeone;
		Edgeone.insert(edge);
		Inverse_Edges.insert(std::pair<std::pair<int,int>,std::set<pair<int,int> > > (key,Edgeone));
	}
}

// Add a cycle to the subgraph
void Node_Info::Add_Cycle(int node, int length, int nextnode, std::vector<int> path){
	Cycle C(node,length,nextnode,path);
	std::map<int,Cycle>::iterator it;
	it=Cycle_by_nextnode.find(nextnode);
	if(it!=Cycle_by_nextnode.end()){
		if (length>(it->second.Getlength())){
			Cycle_by_nextnode.erase(it);
			Cycle_by_nextnode.insert(std::pair<int,Cycle> (nextnode,C));
		}
	}
	else{
		Cycle_by_nextnode.insert(std::pair<int,Cycle> (nextnode,C));
	}
}

// To find if there is an edge (t,v) in the trail of subgraph 
set<pair<int,int> > Node_Info::Find_Inverse_Edges(int t,int v){
	pair<int,int> key(t,v);
	std::map<std::pair<int,int>,std::set<pair<int,int> > >::iterator it;
	it=Inverse_Edges.find(key);
	if(it!=Inverse_Edges.end()){
		return it->second;
	}
	else{
		set<pair<int,int> > set_vide;
		return set_vide; 
	}
}
// The number of cycles in the subgraph
int Node_Info::Number_of_Cycle(){
	return Cycle_by_nextnode.size();
}

// The length of the trail in the subgraph
int Node_Info::Number_of_Inverse_Edges(){
	return Inverse_Edges.size();
}
// To test if there exists a cycle which passes node "nextnode" in the subgraph
int Node_Info::If_Existed_Cycle_by_nextnode(int nextnode){
	std::map<int,Cycle>::iterator it;
	it=Cycle_by_nextnode.find(nextnode);
	if(it!=Cycle_by_nextnode.end()){
		return 1;
	}
	else return 0;
}

// To return such a cycle found above
Cycle* Node_Info::Return_Cycle_by_nextnode(int nextnode){
	std::map<int,Cycle>::iterator it;
	it=Cycle_by_nextnode.find(nextnode);
	return &(it->second);
}

// To return the length of the cycle fond above
int Node_Info::Return_length_Cycle_by_nextnode(int nextnode){
	if(If_Existed_Cycle_by_nextnode(nextnode)){
		return (Return_Cycle_by_nextnode(nextnode))->Getlength();
	}
	else return 0;
}

// To test if there exists a cycle of length which does not pass node "notnextnode"
int Node_Info::If_Existed_Cycle_by_length(int notnextnode,int cyc_not_include){
	if(Number_of_Cycle()>1){return 1;}
	if(Number_of_Cycle()==0){return 0;}
	else{
		std::map<int,Cycle>::iterator it;
		it=Cycle_by_nextnode.find(notnextnode);
		if(it!=Cycle_by_nextnode.end()){
			return 0;
		}
		else{
			return 1;
		}
	}
}
// To return such a cycle described above
Cycle Node_Info::Return_Cycle_by_length(int notnextnode,int cyc_not_include){
	std::map<int,Cycle>::iterator it;
	int length_best=0;
	for(it=Cycle_by_nextnode.begin();it!=Cycle_by_nextnode.end();++it){
		if(it->first!=notnextnode&&it->first!=cyc_not_include){
			if(it->second.Getlength()>length_best){
				length_best=it->second.Getlength();
				return (it->second);
			}
		}
	}
	vector<int> path;
	Cycle C(0,0,0,path);
	return C;
}

