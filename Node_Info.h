#ifndef BASICTYPE_H
#define BASICTYPE_H
#include <map>
#include <vector>
#include <set>

using namespace std;
class Cycle{
	int node;
	int length;
	int nextnode;
	std::vector<int> path;
public:
	Cycle(int node, int length, int nextnode, std::vector<int> path);
	int Getlength();
	std::vector<int> Getpath();
};
class Node_Info{
	int number_node;
	std::map<std::pair<int,int> ,std::set<pair<int,int> > > Inverse_Edges;
	public: //These functions are defined in NodeInfo.cpp
		Node_Info(int number_node); 
		int l1;
		int l2;
		std::map<int,Cycle> Cycle_by_nextnode;
		std::vector<int> T1;
		std::vector<int> T2;
		void Save_Inverse_Edges(int endpoint, int pass, std::pair<int,int> edge);
		void Add_Cycle(int node, int length, int nextnode, std::vector<int> path);
		void Setl1(int v);
		std::set<pair<int,int> >Find_Inverse_Edges(int t,int v);
		int Number_of_Inverse_Edges();
		int Number_of_Cycle();
		int If_Existed_Cycle_by_nextnode(int nextnode);
		Cycle* Return_Cycle_by_nextnode(int nextnode);
		int Return_length_Cycle_by_nextnode(int nextnode);
		int If_Existed_Cycle_by_length(int notnextnode,int cyc_not_include);
		Cycle Return_Cycle_by_length(int notnextnode,int cyc_not_include); 
};
#endif 
