/********After the whole search procedure for longest trail******************
*****There are two codes for post processing the longest trail obtained******
1. Give_Statistique2: returns the nonzeros in the first, second and third subdiagonal in reordered contact map
2. Give_affectation: returns the new assignment to the residues according to the order of nodes in the longest trail
***************************************************************************/
#include "Header.h"
#include "SubfunLT.h"
#include <iostream>
#include <map>
#include <set>
#include <algorithm>
#include <stdio.h>
using namespace std;
vector<int> Give_Statistique2(noeud **Adj,int *v,int m){
	vector<int> Sum(Bound_K+1,0);
	for(int i=1;i<=m;i++){
		for(noeud *t=Adj[i];t!=z1;t=t->suivant){
			if(v[i]<v[t->s]){
				int diff=v[t->s]-v[i];
				if(diff<=Bound_K){
					Sum[diff]++;
				}
			}
		}
	}
	return Sum;
}


vector<int> Give_affectation(int *v,map<int,Node_Info> &map,int number_vertex,int m,noeud **Adj, int *ancestor){
	vector<int> Tree1=map.find(number_vertex)->second.T1;
	vector<int> Tree2=map.find(number_vertex)->second.T2;
	vector<int> mark(m+1,0);
	int length_path=map.find(number_vertex)->second.l1+map.find(number_vertex)->second.l2;
	int sum_v_reach=Tree1.size()+Tree2.size()+1;
	
	vector<int>::iterator it;
	int count=0;
	reverse(Tree1.begin(),Tree1.end());
	for(it=Tree1.begin();it!=Tree1.end();++it){
		count++;
		v[*it]=count;
		mark[*it]=1;
	}
	count++;
	v[number_vertex]=count;
	mark[number_vertex]=1;
	for(it=Tree2.begin();it!=Tree2.end();++it){
		count++;
		v[*it]=count;
		mark[*it]=1;
	}

	set<int> Vertex_to_aff;
	for(int i=1;i<mark.size();i++){
		if(mark.at(i)==0){
			Vertex_to_aff.insert(i);
		}
	}
	int check=Vertex_to_aff.size();
	for(set<int>::iterator it1=Vertex_to_aff.begin();it1!=Vertex_to_aff.end();++it1){
		int father,son;
		son=*it1;
		vector<int> Tree_to_aff;
		Tree_to_aff.insert(Tree_to_aff.begin(),*it1);
		do{
			father=ancestor[son];
			son=father;
			Tree_to_aff.insert(Tree_to_aff.begin(),father);
		}while(Vertex_to_aff.find(father)!=Vertex_to_aff.end());
		Tree_to_aff.erase(Tree_to_aff.begin());

		if(map.find(father)->second.T1.at(0)==Tree_to_aff.at(0)){
			Tree_to_aff.clear();
			Tree_to_aff=map.find(father)->second.T1;
		}
		else{
			if (!map.find(father)->second.T2.empty()){
				if (map.find(father)->second.T2.at(0) == Tree_to_aff.at(0)){
					Tree_to_aff.clear();
					Tree_to_aff = map.find(father)->second.T2;
				}			
			}
			
		}
		if(!Tree_to_aff.empty()) length_path+=Tree_to_aff.size()-1;
		for(vector<int>::iterator ittree=Tree_to_aff.begin();ittree!=Tree_to_aff.end();++ittree){
			count++;
			v[*ittree]=count;
			if(Vertex_to_aff.find(*ittree)!=Vertex_to_aff.end()&&*ittree!=*it1){
				Vertex_to_aff.erase(Vertex_to_aff.find(*ittree));
			}
		}		
	}
	
	vector<int> stat = Give_Statistique2(Adj,v,m);
	return stat;
}
vector<int>::iterator find(vector<int> &v,int f){
	for(vector<int>::iterator it=v.begin();it!=v.end();++it){
		if(f==*it){
			return it;
		}
	}
}

