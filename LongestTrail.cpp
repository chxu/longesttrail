#include "Header.h"
#include "SubfunLT.h"
#include <iostream>
#include <map>
#include <set>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
using namespace std;

/***********************************
***Refer to FindLongestTrail of Figure 2c in the maniscript****
@param t: the node has been visited
@param color: the color of nodes (0:white, 1: gray, 2: black)
@param ancestor:  the ancestors of nodes
@param Adj:  the noisy and permuted contact map
@param val: the assignment to the residues
@param m: the size of contact map
@param map: a map for saving the longest trail of each subgraph (by dynamic programming procedure)

All the operations on the class Node_Info (NodeInfo.h) are given in the script NodeInfo.cpp
******************************************************/
void FindLongestTrail(int t, int *color,int *ancestor, noeud **Adj,int * val,int m,map<int,Node_Info>& Map_Graph){
	noeud *vs;
	Node_Info *T_Info =&(Map_Graph.find(t)->second);
	for(vs=Adj[t];vs!=z1;vs=vs->suivant){ //(line 3 in pseudo code of Figure 2c of maniscript) 
		int v = vs->s;
		Node_Info *V_Info =&(Map_Graph.find(v)->second);
		if(color[v]==2){
			if(ancestor[v]==t){ //line 5
				int l=0;
				vector<int> Tree;
				//line 6 in pseudo code of Figure 2c
				l=V_Info->l1+1;
				Tree.insert(Tree.begin(),V_Info->T1.begin(),V_Info->T1.end());
				Tree.insert(Tree.begin(),v);

				set<pair<int,int> > I_Egdes = T_Info->Find_Inverse_Edges(t,v);
				set<pair<int,int> >::iterator it;
				for(it=I_Egdes.begin();it!=I_Egdes.end();++it){ // line 7 in pseudo code 
					int u=it->first;
					int d; 
					std::vector<int> path;
					int m=nextnode(ancestor,v,u);
					if(!Contains(&(V_Info->T1),m)){//line 8
						int t1_next=V_Info->T1.at(0);
						Calculate_d(v,u,&d,path,Map_Graph,ancestor,t1_next); // line 9 in pseudo code 
						if(d+1+V_Info->l1>l){ //line 10 
								/*****line 11-12*************
								***Corresponds to Situtation II in Figure 1c
								**************************************/
								l=d+1+V_Info->l1;							
								Tree.clear();
								std::vector<int> path_rev = V_Info->T1;
								Tree.insert(Tree.begin(),path_rev.begin(),path_rev.end());
								reverse(path.begin(),path.end()); 
								Tree.insert(Tree.begin(),path.begin(),path.end());
								Tree.insert(Tree.begin(),u);
								reverse(path.begin(),path.end()); 
						}
					}
					else{
						int t2_next;
						if(V_Info->T2.size()>0) t2_next=V_Info->T2.at(0);
						else t2_next=0;
						Calculate_d(v,u,&d,path,Map_Graph,ancestor,t2_next); // line 14
						if(d+1+V_Info->l2>l){ // line 15
								/*****line 16-17 *************
								***Corresponds to Situtation III in Figure 1c
								**************************************/
								l=d+1+V_Info->l2;
								Tree.clear();
								std::vector<int> path_rev = V_Info->T2;
								Tree.insert(Tree.begin(),path_rev.begin(),path_rev.end());
								reverse(path.begin(),path.end());
								Tree.insert(Tree.begin(),path.begin(),path.end());
								Tree.insert(Tree.begin(),u);
								reverse(path.begin(),path.end());
						}
					}

					if ((1+d)>T_Info->Return_length_Cycle_by_nextnode(v)){
						std::vector<int> path_cyc;
						path_cyc.insert(path_cyc.begin(),u);
						path_cyc.insert(path_cyc.begin(),path.begin(),path.end());
						T_Info->Add_Cycle(t,1+d,v,path_cyc);
					}
				}
				if(l>=T_Info->l1){ //line 18-19
					T_Info->l2=T_Info->l1;T_Info->T2=T_Info->T1;
					Map_Graph.find(t)->second.Setl1(l);T_Info->T1=Tree; T_Info->l1 = l;
				}
				else{
					if(l>=T_Info->l2){ //line 21-22
						T_Info->l2=l;T_Info->T2=Tree;
					}
				}
			}	
		}
		

		else{  //line 24-25
			if(color[v]==1){
				if(ancestor[t]!=v){
					V_Info->Save_Inverse_Edges(v,nextnode(ancestor,v,t),pair<int,int>(t,v));		
				}
			}
			else {
				printf("There is an error of FingLongestTrail for the edges\n");
				exit(0);
			}
		}

	}
}

/***********************************
***Refer to DFS_Visit(t) of Figure 2 in maniscript****
@param t: the node has been visited
@param color: the color of nodes (0:white, 1: gray, 2: black)
@param ancestor:  the ancestors of nodes
@param Adj:  the noisy and permuted contact map
@param val: the assignment to the residues
@param m: the size of contact map
@param map: a map for saving the longest trail of each subgraph (by dynamic programming procedure)
******************************************************/

void DFS_Visit(int t, int *color,int *ancestor, noeud **Adj,int * val,int m, map<int,Node_Info> &map){
	color[t]=1;	
	for(noeud *v=Adj[t];v!=z1;v=v->suivant){
		if(color[v->s]==0){
			ancestor[v->s]=t;
			DFS_Visit(v->s,color,ancestor,Adj,val,m,map);
		}
	}
	color[t]=2;
	FindLongestTrail(t,color,ancestor,Adj,val,m,map);
}

/***** Main algorithm (Refer to DFS(G) of Figure 2 in manuscript) *******
Calculates the longest trail in a contact map presented by Adj*********
@param Adj: input contact map (noisy and permuted contact map)*******
@param m: the size of contact map
@param number_begin: the root node to start depth first search procedure
@param &val: new assignment to the residues obtained by the program 
@output results: the number of elements in the first, second and third diagonal of the reordered contact map
*******************************************************/

vector<int> LongestTrailAlgo(noeud **Adj,int m, int number_begin,int *val){
	//Initilization
	int *color=new int[m+1];
	int *ancestor=new int[m+1];
	for(int i=1;i<=m;i++){
		color[i]=0; 
		ancestor[i]=0;
	}

	map<int,Node_Info> Map_Graph; //Node_Info is a class defined in Node_Info.h, all the operations on Node_Info are defined in NodeInfo.cpp
	for(int i=1;i<=m;i++){
		Node_Info info(i);
		Map_Graph.insert(pair<int,Node_Info>(i,info));
	}
	
	//Classical Depth First Search procedure, refer to DFS(G) of Figure 2 in manuscript
	for(int i=1;i<=m;i++){
		int number_vertex = number_begin+i-1;
		if(number_vertex>m){
			number_vertex = number_vertex-m;
		}
		if(color[number_vertex]==0){
			DFS_Visit(number_begin,color,ancestor,Adj,val,m,Map_Graph);
		}
	}

	for(int i=1;i<=m;i++){
		val[i]=0;
	}

	vector<int> results = Give_affectation(val,Map_Graph,number_begin,m,Adj,ancestor);
	return results; 
}

