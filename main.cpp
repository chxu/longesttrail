/****************************************
# Chuan Xu
# February 2018
# Longest trail algorithm
# Command line 	arguments:
	./protein inputfile(*.txts) noise_percentage number_of_runs

Command parameters 
@param inputfile.txts: the input contact map (matrix A)
@param noise_percentage: the percentage of noise introduced to the input contact map 
@param number_of_runs: the number of runs on the same input contact map
@return outputfile.txt: the output contact map obtained by runing longest trail algorithm (matrix B)
****************************************/


#include<iostream>
#include <fstream>
#include <omp.h>
#include <iomanip>  
#include "Header.h"
#include "MatrixProcess.h"
#include <set>
#include <time.h>
#include <string.h>
#include <stdlib.h>   
using namespace std;


noeud *z1;
int debut, fin;
int main(int argc, char *argv[]){
	//Read user's parameters
	if (argc < 4)
	{
		fprintf(stderr, "Error of input command \nUsage: %s inputfile(*.txts) noise_percentage number_of_runs\n", argv[0]);
		exit(1);
	}
	cout<<"--------------------------------------------"<<endl;
	cout << "Input matrice is in file:" << argv[1] << endl;
	float percent = atof(argv[2]);
	if (percent >=1.0 )
	{
		fprintf(stderr, "Error: the percentage of noise given by the command line should be <= 1\n");
		exit(1);
	}
	cout << "Noise percentage:" << percent << endl;
	int testnumber = atoi(argv[3]);
	cout << "Number of runs:" << testnumber << endl;
	//Important variables:
	/***********************************************************************
	@param m: the size of the input contact map (CM)
	@param nz: non zero elements in CM
	@param CheckedEdgeGlobal:  the graph corresponds to input CM (matrix A)
				  (a map that connects each node with its adjacent edges)
	@param CheckEdge:  the graph corresponds to noisy CM (matrix A')
				  (a map that connects each node with its adjacent edges)
	@param Adj2: the graph corresponds to noisy and permuted CM (matrice A'')
		    (The structure of Adj2 is a list of "noeud" which is defined in Header.h)

	************************************************************************/
	int m, nz;
	noeud *Adj2[1000];
	int affectation[1000]; 
	int affectation_best[1000];
	z1 = (noeud*)malloc(sizeof *z1);
	z1->s = 10000;
	z1->suivant = z1;

	/***********************************************
	*****1. Read input contact map **********************	
	CheckedEdgeGlobal: initialized to the origin contact map (matrix A)
	*************************************************/	
	FILE *f = NULL;
	std::map<int, std::set<int> > CheckedEdgeGlobal = OpenFile(f, argv, &m, &nz); 
	std::vector<int> affectation_try(m + 1, 0);
	for (int i = 1; i <= m; i++){
		affectation[i] = i;
		affectation_try[i] = i;
	}
	
	
	//Prepare the outfile.txt to note down the output contact map	
	char filename[50];
	char * split = strtok(argv[1], ".");
	sprintf(filename, "%s_%d_%drun.txt", split, (int) (percent*100),testnumber);
	ofstream outputfile;
	outputfile.open(filename);

	//WritetoFile(outputfile, CheckedEdgeGlobal, affectation_try, affectation); //Write to the ouptfile the origin contact map
	
	for (int runtime = 1; runtime <= testnumber; runtime++){

		cout << "\nRun:" << runtime << endl;

		
		/*************************************************
		*****2. Noise introduction **********************	
		CheckedEdge: initialized to the noisy contact map (matrix A')
		*************************************************/	
		std::map<int, std::set<int> > CheckedEdge(CheckedEdgeGlobal.begin(), CheckedEdgeGlobal.end()); 
		NoiseIntroduction(m, percent, CheckedEdge);   // Defined in Openfile.cpp
		//WritetoFile(outputfile, CheckedEdge, affectation_try, affectation); //Write to the ouptfile the noisy contact map

		
		/*************************************************
		*****3. Random Permutation **********************	
		Adj2: initilized to the noisy and permuted contact map (matrix A'')
		*************************************************/	
		std::vector<int> affectation_random(m + 1, 0);
		affectation_random = RandomPermutation(Adj2, CheckedEdge, m); // Defined in Openfile.cpp
		
		/**Write to the ouptfile the noisy and permuted contact map
		int affectation_random_per[1000];
		for (int i = 1; i <= m; i++){
				affectation_random_per[i]= affectation_random[i];
		}
		WritetoFile(outputfile, CheckedEdge, affectation_try, affectation_random_per);
		*/
		
		/******************************************************
		******4. Longest trail algorithm on ************************
		***input: noisy and permuted contact map (matrix A'')**********
		***ouput: reordered contact map saved to output file
 		******************************************************/	
		int LongestTrail = 0; // Global optimum of longest path
		vector<int> BestAdapt(Bound_K + 1, 0);	

		for (int i = 1; i <= m; i = i + 1){
			
			/***** 4.1.Main procedure: LongestTrailAlgo ***********
			@input: matrix A''*************
			@output: 
				affectation: new assignment of each residue
				Results: the number of elements in the first, second and third diagonal of reordered contact map
					where Result.at(1) is the length of the trail.
			*******************************************************/
			vector<int> Result = LongestTrailAlgo(Adj2, m, i, affectation); // Defined in LongestTrail.cpp



			/********************************************************
			* 4.2. Save the best assignment (longest trail) until now*
			*********************************************************/		
			int result = Result.at(1); // Take the length of the trail
			if (result >= LongestTrail){
				if (result > LongestTrail){
					LongestTrail = result;
					BestAdapt.assign(Result.begin(), Result.end());
					memcpy(affectation_best, affectation, sizeof(affectation));
				}
				else{
					int flag = 0;
					for (int c = 2; c <= Bound_K; c++){
						if (Result.at(c) >= BestAdapt.at(c)){
							BestAdapt.assign(Result.begin(), Result.end());
							memcpy(affectation_best, affectation, sizeof(affectation));
							flag = 1;
							break;
						}
						else{
							if (Result.at(c) < BestAdapt.at(c)){
								flag = 1;
								break;
							}
						}
					}
					if (!flag){
					}
				}
			}

		}

		/********************************************************
		* 4.3. Save the reordered matrix to output file ********
		*********************************************************/
		WritetoFile(outputfile, CheckedEdge, affectation_random, affectation_best);

		cout<<"The reordered matrix has :\n   " << LongestTrail<< " non-zeros in the first sub-diagonal, \n   " << BestAdapt.at(2)<< " non-zeros in the second sub-diagonal, \n   "<< BestAdapt.at(3) << " non-zeros in the third sub-diagonal"<< endl;	
		cout<<"------------------------------------------------------------------\n";
		affectation_random.clear();

	}
	cout<<"End of execution: the results (reordered matrices) are written to the file:" <<filename<<endl;
	outputfile.close();

	return 1;
}
